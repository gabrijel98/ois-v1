import java.util.*;

public class Gravitacija{

    public static final double C =6.674*Math.pow(10,-11);
    public static final double M=5.972*Math.pow(10,24);
    public static final double R=6.371*Math.pow(10,6);

    public static void main(String[] args) {
        System.out.println("OIS je zakon!");

        Scanner sc = new Scanner(System.in);

        double nadmorskaVisina = sc.nextDouble();

        
        izpis(nadmorskaVisina, izracun(nadmorskaVisina));

        
    }
    
    public static double izracun(double nadmorskaVisina){
        return (C*M)/(Math.pow((R + (nadmorskaVisina * 1000)),2));
    }

    
    public static void izpis(double nadmorskaVisina, double izracun){
        
        
        System.out.printf("Gravitacijski pospešek na %.2f nadmorske višine je %.2f m/s^2\n", nadmorskaVisina , izracun(nadmorskaVisina) );
        
    }
    
}